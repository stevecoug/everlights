#!/usr/bin/env php
<?php

use stevecoug\Everlights\Controller;
use stevecoug\Everlights\Sequence;

require_once __DIR__."/vendor/autoload.php";

function usage() {
	echo "Usage: everlights.php <sequence|off>\n";
	echo "\n";
	echo "Possible sequences:\n";
	foreach (array_keys($GLOBALS['settings']['sequences']) as $seq) {
		echo "\t- {$seq}\n";
	}
	echo "\n";
	exit();
}


$settings = json_decode(file_get_contents(__DIR__."/settings.json"), true);
if (!$settings) {
	echo "ERROR: Invalid JSON file\n";
	exit(1);
}

$box = new Controller($settings['controller']);




if (!isset($argv[1])) usage();
$seq_name = $argv[1];

if ($seq_name === "off") {
	$box->off();
	exit();
}

if (!isset($settings['sequences'][$argv[1]])) usage();

if (isset($argv[2])) {
	$num = intval($argv[2]);
	if (!isset($settings['sequences'][$seq_name][$num])) usage();
} else {
	$num = false;
}


$group = $settings['sequences'][$seq_name];
if ($num === false) {
	$num = mt_rand(0, count($group) - 1);
}
$seq = new Sequence($settings['sequences'][$seq_name][$num], $settings['colors'], $settings['effects']);

$box->sequence($seq);
