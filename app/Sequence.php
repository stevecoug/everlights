<?php

namespace stevecoug\Everlights;

class Sequence {
	private $colors = [];
	private $effects = [];
	
	public function __construct(array $def, array $all_colors, array $all_effects) {
		if (isset($def['multiply'])) {
			$mult = intval($def['multiply']);
		} else {
			$mult = 1;
		}

		$this->colors = [];
		foreach ($def['colors'] as $color) {
			for ($i = 0; $i < $mult; $i++) {
				if (!isset($all_colors[$color]) || !is_array($all_colors[$color]) || count($all_colors[$color]) != 3) {
					throw new \Exception("Invalid color: $color");
				}

				list($r, $g, $b) = $all_colors[$color];
				$this->colors[] = new Color($r, $g, $b);
			}
		}
		
		$this->effects = [];
		foreach ($all_effects as $effect => $short) {
			if (isset($def['effects']) && isset($def['effects'][$effect])) {
				$this->effects[$short] = intval($def['effects'][$effect]);
			}
		}
	}

	public function get_colors() {
		return $this->colors;
	}

	public function get_effects() {
		return $this->effects;
	}
}

