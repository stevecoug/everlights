<?php

namespace stevecoug\Everlights;

// GET /ptrn/set?channel=3&data=%7B%22len%22:3,%22ptrn%22:%5B16777215,0,0%5D,%22modes%22:%7B%22fa%22:0,%22bl%22:0,%22st%22:0,%22cf%22:0,%22cr%22:0,%22wi%22:0,%22ra%22:0%7D,%22name%22:%22%22%7D HTTP/1.1\r\n
// {"len":3,"ptrn":[16777215,16711680,0],"modes":{"fa":0,"bl":0,"st":0,"cf":22,"cr":0,"wi":0,"ra":0},"name":""}
// channels 0-3
// GET /ptrn/clear?channel=3 HTTP/1.1\r\n


class Controller {
	private $hostname = false;
	
	public function __construct(string $hostname) {
		$this->hostname = $hostname;
	}
	
	private function log($msg) {
		if (posix_isatty(STDIN)) {
			echo "$msg\n";
		} else {
			$log = sprintf("[%s] %s\n", date("Y-m-d H:i:s"), $msg);
			file_put_contents("/tmp/everlights.log", $log, FILE_APPEND);
		}
	}

	public function sequence(Sequence $seq) {
		$colors = $seq->get_colors();
		$effects = $seq->get_effects();

		$arr = [
			"len" => count($colors),
			"ptrn" => [],
			"modes" => [],
			"name" => "",
		];
		
		foreach ($colors as $color) {
			$arr['ptrn'][] = $color->value();
		}
		foreach ($effects as $effect => $value) {
			$arr['modes'][$effect] = $value;
		}

		$data = json_encode($arr);
		$this->log($data);
		$str = sprintf("/ptrn/set?channel=3&data=%s", urlencode($data));
		$this->send($str);
	}

	public function off() {
		for ($i = 0; $i <= 3; $i++) {
			$this->send("/ptrn/clear?channel=$i");
		}
	}
	
	private function send($msg) {
		$this->log($msg);
		file_get_contents("http://$this->hostname$msg");
		usleep(10000);
	}
}

