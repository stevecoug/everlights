<?php

namespace stevecoug\Everlights;


class Color {
	private $red = 0;
	private $green = 0;
	private $blue = 0;

	public function __construct(int $red, int $green, int $blue) {
		if ($red < 0 || $green < 0 || $blue < 0) {
			throw new \Exception("Color values cannot be below 0");
		}
		if ($red > 255 || $green > 255 || $blue > 255) {
			throw new \Exception("Color values cannot be above 255");
		}

		$this->red = $red;
		$this->green = $green;
		$this->blue = $blue;
	}
	
	public function value() {
		return ($this->red * 65536) + ($this->green * 256) + ($this->blue);
	}
}

